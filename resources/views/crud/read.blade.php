@extends('layout')

@section('title', 'Lista Utenti')
    
@section('content')

    <h1>Lista Utenti</h1>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead>
                        <td>ID</td>
                        <td>Nome</td>
                        <td>Email</td>
                        <td>Password</td>
                        <td><a href="{{route('crea-utente')}}" class="btn btn-success">Add New</a>
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
    
                                <a href="route('logout')" class="btn btn-success"
                                        onclick="event.preventDefault();
                                                    this.closest('form').submit();">
                                    LogOut
                                </a>
                            </form>
                        </td>
                    </thead>
                    @foreach ($utenti as $utente)
                        <tbody>
                            <td>{{$utente->id}}</td>
                            <td>{{$utente->name}}</td>
                            <td>{{$utente->email}}</td>
                            <td>{{$utente->password}}</td>
                            <td><a href="{{route('aggiorna-utente',$utente->id)}}" class="btn btn-success">Aggiorna</a>
                                <a href="{{route('elimina-utente',$utente->id)}}" class="btn btn-warning">Elimina</a></td>
                        </tbody>
                    @endforeach
                </table>
                {{$utenti->links('pagination::bootstrap-4')}}
            </div>
        </div>
    </div>

@endsection