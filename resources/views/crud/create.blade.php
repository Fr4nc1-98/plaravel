@extends('layout')

@section('title', 'Inserisci Utenti')

@section('content')

<h1>Aggiungi Utenti</h1>

<div class="container">
    <div class="row">
        <div class="col-12">

            <form action="" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="text" name="nome" class="form-control" placeholder="Inserisci nome">
                <input type="email" name="email" class="form-control" placeholder="Inserisci email">
                <input type="password" name="password" class="form-control" placeholder="Inserisci password">
                <input type="submit" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>

@endsection