@extends('layout')

@section('title', 'Aggiorna utenti')

@section('content')
    <h1>Aggiorna utenti</h1>

    <div class="container">
        <div class="row">
            <div class="col-12">
    
                <form action="" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" name="nome" class="form-control" placeholder="Inserisci nome" value="{{$utenti->name}}">
                    <input type="email" name="email" class="form-control" placeholder="Inserisci email" value="{{$utenti->email}}">
                    <input type="password" name="password" class="form-control" placeholder="Inserisci età" value="{{$utenti->password}}">
                    <input type="submit" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
@endsection