<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Utenti;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utenti = Utenti::orderBy('id','ASC')->paginate(4);
        return view('crud.read',['utenti' => $utenti]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $utenti = new Utenti;
        $utenti->name = $request->nome;
        $utenti->email = $request->email;
        $utenti->password = $request->password;
        $utenti->save();

        return redirect ('dashboard');
    }

    public function updateRead($id)
    {
        $utenti = Utenti::find($id);
        return view('crud.update',['utenti' => $utenti]);
    }  

    public function update(Request $request, $id)
    {
        $utenti = Utenti::find($id);
        $utenti->name = $request->nome;
        $utenti->email = $request->email;
        $utenti->password = $request->password;
        $utenti->save();

        return redirect('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Utenti::destroy($id);
        return redirect('dashboard');
    }
}
