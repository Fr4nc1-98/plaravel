<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::redirect('/dashboard', '/read')->middleware(['auth']);

Route::get('/read', [UsersController::class, 'index'])->middleware(['auth'])->name('dashboard');

// create
Route::view('/create', 'crud.create')->middleware(['auth'])->name('crea-utente');
Route::post('/create', [UsersController::class, 'create'])->middleware(['auth']);

// update
Route::get('/update/{id}', [UsersController::class, 'updateRead'])->middleware(['auth'])->name('aggiorna-utente');
Route::post('/update/{id}', [UsersController::class, 'update'])->middleware(['auth']);

// delete
Route::get('/delete/{id}', [UsersController::class, 'destroy'])->middleware(['auth'])->name('elimina-utente');

require __DIR__.'/auth.php';
